﻿using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.PL
{
    public class GuestMenu : BaseMenu, IAuthorization
    {

        public GuestMenu(UnitOfWork uow): base(uow) 
        {
            Commands = new Dictionary<int, Action>()
            {
                { 1, PrintPoductList },
                { 2, SearchProduct },
                { 3, CreateAccount },
                { 4,  LogIn}
            };
        }
        public override void PrintMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-- Menu");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("1. Show all products");
            Console.WriteLine("2. Search product by name");
            Console.WriteLine("3. Sign Up");
            Console.WriteLine("4. Sign In");
        }

        public override void GetUserInput(string data) 
        {
            int parsed=0;
            int.TryParse(data,out parsed);
            if (Commands.ContainsKey(parsed))
            {
                Commands[parsed].Invoke();
            }
        }
        public override void Execute() 
        {
            PrintMenu();
            Console.Write("Your choice: ");
            GetUserInput(Console.ReadLine());
        }

        public override void PrintPoductList()
        {
            base.PrintPoductList();
            Console.WriteLine("Press any key back to menu...");
            Console.ReadKey();
        }

        public void CreateAccount() 
        {
            Console.Clear();
            Console.WriteLine("--Creation of new account --");
            Console.Write("Enter login: ");
            var login = Console.ReadLine();
            Console.Write("Enter password: ");
            var password = Console.ReadLine();
            var newUser = SignUp(login, password);
            try
            {
                UserService.Register(newUser);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }

            LoginNotify?.Invoke(newUser, new EventArgs());
        }

        public void LogIn() 
        {
            Console.WriteLine("-- Log In --");
            Console.Write("Enter login: ");
            var login = Console.ReadLine();
            Console.Write("Enter password: ");
            var password = Console.ReadLine();

            User user=null;
            try
            {
                user = UserService.LogIn(login, password);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return;
            }
          
            LoginNotify?.Invoke(user, new EventArgs());
        }

        public bool SignIn(string login, string password)
        {
            throw new NotImplementedException();
        }

        public User SignUp(string login, string password)
        {
            return new RegisteredUser() {Login=login, Password = password, Permission= UserPermission.RegisteredUser};
        }
    }
}
