﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.UoW;

namespace ConsoleEShopOnion.PL
{
    public class AdminMenu : UserMenu<RegisteredUser>
    {
        public AdminMenu(UnitOfWork uow, RegisteredUser admin) : base(uow,admin)
        {
            Commands = new Dictionary<int, Action>()
            {
                { 1, PrintPoductList },
                { 2, SearchProduct },
                { 3, ShowAndCreate },
                { 4, ShowAllUsers},
                { 5, AddingNewProduct},
                { 6, ChangeProducts},
                { 7, ChangeOrderStatus},
                { 8, LogOut}
            };
        }
        public override void PrintMenu()
        {
            Console.WriteLine($"You loggined as {user.Login}");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-- Menu");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("1. Show all products");
            Console.WriteLine("2. Search product by name");
            Console.WriteLine("3. Create new order");
            Console.WriteLine("4. Viewing and changing personal information of users");
            Console.WriteLine("5. Adding new products");
            Console.WriteLine("6. Change of product information");
            Console.WriteLine("7. Change order status");
            Console.WriteLine("8. Log out");
        }

        public override void GetUserInput(string data)
        {
            int parsed;
            int.TryParse(data,out parsed);
            if (Commands.ContainsKey(parsed))
            {
                Commands[parsed].Invoke();
            }
        }

        public void ChangeOrderStatus()
        {

            //var orders = OrderService.Get();
            //do
            //{
            //    Console.Clear();
            //    Console.WriteLine("\t\t\t\t\t --- Orders list ---");
            //    foreach (var i in orders)
            //    {
            //        Console.WriteLine();
            //        Console.WriteLine(i);
            //    }
            //    Console.WriteLine("\n1 - Change order status ");
            //    Console.WriteLine("2 - Back to menu");
            //    Console.Write("Enter: ");
            //    int choice;
            //    int.TryParse(Console.ReadLine(), out choice);
            //    switch (choice)
            //    {
            //        case 1:
            //            Console.Write("Enter order number to change status: ");
            //            int id; int.TryParse(Console.ReadLine(), out id);
            //            var order = OrderService.Find(i=> i.Id==id);
            //            if (order == null)
            //            {
            //                Console.WriteLine($"Order with number: {id} is not exist!");
            //                Console.ReadKey();
            //                break;
            //            }
            //            SetOrderStatus(order);
            //            break;

            //        case 2: return;
            //        default: break;
            //    }


            //} while (true);
        }

        public void SetOrderStatus(Order order)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("\t\t\t\t\t --- Order ---");
                Console.WriteLine(order);
                Console.WriteLine("\n1 - Set status \"Canceled by Admin\" ");
                Console.WriteLine("2 - Set status \"Received payment\" ");
                Console.WriteLine("3 - Set status \"Sent\" ");
                Console.WriteLine("4 - Set status \"Completed\" ");
                Console.WriteLine("5 - Back to menu");
                Console.Write("Enter: ");
                int choice;
                int.TryParse(Console.ReadLine(), out choice);
                bool success = false;
                switch (choice)
                {
                    case 1:
                        order.OrderStatus = OrderStatus.CanceledByAdmin;
                        success = true;
                        break;
                    case 2:
                        if (order.OrderStatus != OrderStatus.CanceledByAdmin || order.OrderStatus != OrderStatus.CanceledByUser
                            || order.OrderStatus != OrderStatus.Sent)
                        {
                            order.OrderStatus = OrderStatus.ReceivedPayment;
                            success = true;
                        }
                        break;
                    case 3:
                        if (order.OrderStatus != OrderStatus.CanceledByAdmin || order.OrderStatus != OrderStatus.CanceledByUser
                            || order.OrderStatus != OrderStatus.Completed)
                        {
                            order.OrderStatus = OrderStatus.Sent;
                            success = true;
                        }
                        break;
                    case 4:
                        if (order.OrderStatus != OrderStatus.CanceledByAdmin || order.OrderStatus != OrderStatus.CanceledByUser
                            || order.OrderStatus != OrderStatus.New)
                        {
                            order.OrderStatus = OrderStatus.Completed;
                            success = true;
                        }
                        break;
                    case 5: return;
                    default: break;
                }
                if (success)
                {
                    Console.WriteLine("Status successfully changed!");
                    Console.ReadKey();
                }
                else 
                {
                    Console.WriteLine("You can't set this status!");
                    Console.ReadKey();
                }

            } while (true);

        }

        public void ChangeProducts()
        {
            var products = ProductService.GetAll();
            do
            {
                Console.Clear();
                Console.WriteLine("\t\t\t\t\t --- Product list ---");
                foreach (var i in products)
                {
                    Console.WriteLine();
                    Console.WriteLine(i);
                }
                Console.WriteLine("\n1 - Change product ");
                Console.WriteLine("2 - Back to menu");
                Console.Write("Enter: ");
                int choice;
                int.TryParse(Console.ReadLine(), out choice);
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter product ID to change: ");
                        int id; int.TryParse(Console.ReadLine(), out id);
                        ChangeProduct(products.Find(i => i.Id == id));
                        break;
                    case 2: return;
                    default: break;
                }


            } while (true);
        }


        public void ShowAllUsers()
        {
            var users = UserService.Get().ToList();

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\t\t\t\t --- User List ---");
                Console.ForegroundColor = ConsoleColor.White;
                int indx = 1;
                foreach (var user in users)
                {
                    Console.WriteLine($"{indx++}. - {user}");
                }
                Console.WriteLine("1 - Change user info");
                Console.WriteLine("2 - Back to menu");
                Console.Write("Enter: ");
                int choice;
                int.TryParse(Console.ReadLine(), out choice);
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter user ID: ");
                        int id = int.Parse(Console.ReadLine());
                        if (!users.Exists(i => i.Id == id))
                        {
                            Console.WriteLine("User with such ID is not exist!");
                            Console.ReadKey();
                            break;
                        }
                        EditUserInfo(users.Find(i => i.Id == id));
                        break;
                    case 2: return;
                    default: break;
                }
            } while (true);

            Console.ReadLine();
        }

        public void AddingNewProduct()
        {

            do
            {
                Console.Clear();
                Console.WriteLine("\t\t\t\t --- Adding new product ---");
                Console.WriteLine("1 - Add new product");
                Console.WriteLine("2 - Back to menu");
                Console.Write("Enter: ");
                int choice;
                int.TryParse(Console.ReadLine(), out choice);
                switch (choice)
                {
                    case 1: AddProductToDB(); break;
                    case 2: return;
                    default: break;
                }



            } while (true);

        }

        public void AddProductToDB()
        {
            var Product = new Product();
            ChangeProduct(Product);
        }

        private void ChangeProduct(Product Product)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("--Product");
                Console.WriteLine(Product);
                Console.WriteLine("\n1 - Name");
                Console.WriteLine("2 - Price");
                Console.WriteLine("3 - Category");
                Console.WriteLine("4 - Description");
                Console.WriteLine("5 - Save Changes");
                Console.WriteLine("6 - Exit");
                Console.Write("Enter: ");
                int choice;
                int.TryParse(Console.ReadLine(), out choice);
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter name: ");
                        Product.ProductName = Console.ReadLine();
                        break;
                    case 2:
                        Console.Write("Enter Price: ");
                        var price = Console.ReadLine();
                        if (!char.IsNumber(price, 0))
                        {
                            break;
                        }
                        Product.Price = decimal.Parse(price);
                        break;
                    case 3:
                        Console.Write("Enter category: ");
                        Product.Category = Console.ReadLine();
                        break;
                    case 4:
                        Console.Write("Enter description: ");
                        Product.Description = Console.ReadLine();
                        break;
                    case 5:
                        Console.Write("Save changes and add product to DB(y/n): ");
                        var save = Console.ReadLine();
                        if (save == "y")
                        {
                            // TODO: Re-write
                            //if (ProductService.Find(i=> i.Equals(Product))!=null)
                            //{
                            //    Console.WriteLine("Product with such parameters already exists!!");
                            //    Console.ReadKey();
                            //    break;
                            //}
                            ProductService.Add(Product);
                            Console.WriteLine("You have successfully added a new product!");
                            Console.ReadKey();
                            return;
                        }
                        break;
                    case 6: return;
                    default: break;
                }
            }
            while (true);
        }

    }
}
