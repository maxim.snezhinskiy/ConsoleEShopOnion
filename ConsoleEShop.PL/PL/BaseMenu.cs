﻿using ConsoleEShopOnion.BLL.Interfaces;
using ConsoleEShopOnion.BLL.Services;
using ConsoleEShopOnion.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.PL
{

    public abstract class BaseMenu : IMenu
    {

        public EventHandler LoginNotify { get; set; }
        public EventHandler LogOutNotify { get; set; }
        public Dictionary<int, Action> Commands { get; set; }

        protected IOrderService OrderService { get; }
        protected IProductService ProductService { get; }
        protected IUserService UserService { get; }

        public BaseMenu(UnitOfWork uow)
        {
            OrderService = new OrderService(uow);
            ProductService = new ProductService(uow);
            UserService = new UserService(uow);
        }
        public abstract void PrintMenu();
        public abstract void Execute();
        public abstract void GetUserInput(string data);


        public void SearchProduct()
        {
            Console.Clear();
            Console.Write("\nEnter product name: ");
            var entered = Console.ReadLine();
            var product = ProductService.Find(i=>i.ProductName==entered);
            if (product != null)
            {
                Console.WriteLine("\n" + product);
            }
            else
            {
                Console.WriteLine("No product found!");
            }
            Console.WriteLine("\nPress any key go back to menu...");
            Console.ReadLine();
        }


        public virtual void PrintPoductList()
        {
            Console.Clear();
            Console.WriteLine("--- Products ---");
           var data =ProductService.GetAll();
            int indx = 1;
            foreach (var i in data)
            {
                Console.WriteLine($"{indx++}. {i.ToString()}\n");

            }
            Console.WriteLine();
        }
    }
}
