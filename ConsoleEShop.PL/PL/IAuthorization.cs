﻿using ConsoleEShopOnion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.PL
{
    public interface IAuthorization
    {
        User SignUp(string login, string password);
        bool SignIn(string login, string password);
    }
}
