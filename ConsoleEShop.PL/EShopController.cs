﻿using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.UoW;
using ConsoleEShopOnion.PL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion
{
    public  class EShopController
    {
        public  User currentUser { get; set; }
        public  IMenu menu { get; set; }

        private UnitOfWork uow;

        public EShopController(UnitOfWork uow)
        {
            this.uow = uow;
        }
        public  void Start() 
        {
            menu = new GuestMenu(uow);
            while (true)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\t\t\t\t------ ConsoleEShop-Low ------");
                Console.ResetColor();

                menu.LoginNotify += LogIn;
                menu.Execute();
            }
        }

        public  void LogIn(object sender, EventArgs e)
        {
            currentUser = sender as User;
            if (currentUser.Permission == UserPermission.RegisteredUser)
            {
                menu = new UserMenu<RegisteredUser>(uow, currentUser as RegisteredUser);
                menu.LogOutNotify += LogOut;
            }
            if (currentUser.Permission == UserPermission.Admin)
            {
                menu = new AdminMenu(uow, currentUser as RegisteredUser);
                menu.LogOutNotify += LogOut;
            }
        }

        public  void LogOut(object sender, EventArgs e)
        {
            menu = new GuestMenu(uow);
        }

    }
}
