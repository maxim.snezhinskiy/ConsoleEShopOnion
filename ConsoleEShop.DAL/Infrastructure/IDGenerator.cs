﻿using ConsoleEShopOnion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShopOnion.DAL.Infrastructure
{
    public class IDGenerator
    {
        public static int GenerateID<T>(IEnumerable<T> collection) where T: IEntity 
        {
            int maxId = 0;
            if (collection.Any())
            {
                maxId = collection.Max(i => i.Id);
            }
            return maxId + 1;
        }
    }
}
