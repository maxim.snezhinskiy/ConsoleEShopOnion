﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShopOnion.DAL.Entities
{
    public class RegisteredUser: User
    {
        public List<int> OrdersId { get; set; }  = new List<int>();

        public RegisteredUser():base() {
            Permission = UserPermission.RegisteredUser;
        }
        public bool AddNewOrder(int orderID) 
        {
            if (OrdersId.Contains(orderID))
            {
                return false;
            }    
            OrdersId.Add(orderID);
            return true;
        }

        public virtual bool SetStatusCanceled(Order order) 
        {
            if (!OrdersId.Contains(order.Id))
                return false;

            if (order.OrderStatus != OrderStatus.Completed
                || order.OrderStatus != OrderStatus.CanceledByAdmin) 
            {
                order.OrderStatus = OrderStatus.CanceledByUser;
                return true;
            }

            return false;
        }


        public virtual bool SetStatusReceived(Order order)
        {
            if (!OrdersId.Contains(order.Id))
                return false;

            if (order.OrderStatus != OrderStatus.Completed
                || order.OrderStatus != OrderStatus.New
                || order.OrderStatus != OrderStatus.CanceledByAdmin)
            {
                order.OrderStatus = OrderStatus.Completed;
                return true;
            }

            return false;
        }


        public override string ToString()
        {
            return base.ToString();
        }

    }
}
