﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.DAL.Entities
{
    public class OrderItem
    {
        public int Quantity { get; set; }
        public Product Product { get; set; }
        public override string ToString()
        {
            return $"\n-{Product}\nQuantity: {Quantity}";
        }


    }
}
