﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShopOnion.DAL.Entities
{
    public class Order : IEntity
    {
        public static int ID=0;
        public Address OrderAddress { get; set; } = new Address();
        public List<OrderItem> Items { get; set; } = new List<OrderItem>();
        public OrderStatus OrderStatus { get; set; }
        public bool isConfirmed { get; set; }

        public int UserID { get; set; }

        public int Id { get; set; }



        public override string ToString()
        {
            string items = "";
            foreach (var i in Items)
            {
                items += i + "\n";
            }
            return $"-- Order №{this.Id}--\n" +
                   $"{items}\n" +
                   $"Order status: {OrderStatus.ToString()}\n" +
                   $"{OrderAddress}" +
                   $"\n-- Total price: {Items.Sum(i => i.Product.Price * i.Quantity)}\n";
        }
    }
}
