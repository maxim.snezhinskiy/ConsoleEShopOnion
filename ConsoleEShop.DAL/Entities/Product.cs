﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.DAL.Entities
{
    public class Product : IEntity
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        private static int ID = 0;
        public int Id { get; set; }

        public Product()
        {
            ID++;
            Id = ID;
        }
        public override string ToString()
        {
            return $"Product ID: {Id} \nName: {ProductName}, price: {Price}$\nDescription: {Description},\nCategory: {Category ?? "Unknown"} ";
        }
    }
}
