﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.DAL.Entities
{
    public class User : IEntity
    {
        public string Name { get; set; }
        private static int ID = 0;
        public UserPermission Permission { get;   set; }

        public Contact contact { get; set; } = new Contact();
        public string Login { get; set; }
        public string Password { get; set; }

        public int Id { get;  set; }

        protected User() 
        {
            ID++;
            Id = ID;
        }

        public override string ToString()
        {
            return $"User ID: {Id}\n" +
                $"Name: {Name ?? "Unknown"}\n" +
                $"Login: {Login}\n" +
                $"{contact?.ToString()}\n";
        }

    }
}
