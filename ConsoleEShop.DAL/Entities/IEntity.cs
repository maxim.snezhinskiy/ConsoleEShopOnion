﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.DAL.Entities
{
   public interface IEntity
    {
        public int Id { get;}
    }
}
