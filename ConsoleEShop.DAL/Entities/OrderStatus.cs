﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.DAL.Entities
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        CanceledByUser,
        ReceivedPayment,
        Sent,
        Completed
    }
}
