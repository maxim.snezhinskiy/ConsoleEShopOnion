﻿using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.DAL.UoW
{
    public interface IUnitOfWork
    {
        public IRepository<Order> Orders { get;}
        public IRepository<User> Users { get;}
        public IRepository<Product> Products { get;}
    }
}
