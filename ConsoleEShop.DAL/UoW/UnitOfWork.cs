﻿using ConsoleEShopOnion.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopOnion.DAL.Entities;

namespace ConsoleEShopOnion.DAL.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext db;
        private OrderRepository orderRepository;
        private UserRepository userRepository;
        private ProductRepository productRepository;


        public UnitOfWork() 
        {
            db = new DbContext();
        }

        public IRepository<Order> Orders 
        {
            get
            {
                if (orderRepository == null)
                    orderRepository = new OrderRepository(db);
                return orderRepository;
            }

        }

        public IRepository<User> Users 
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(db);
                return userRepository;
            }

        }

        public IRepository<Product> Products 
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(db);
                return productRepository;
            }

        }
    }
}
