﻿using ConsoleEShopOnion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion
{
    public class DbContext
    {
        public virtual List<User> Users { get; set; }
        public virtual List<Order> Orders { get; set; }
        public virtual List<Product> Products { get; set; }

        public DbContext() 
        {
            Users = DataSource.Users;
            Orders = DataSource.Orders;
            Products = DataSource.Products;
        }
    }
}
