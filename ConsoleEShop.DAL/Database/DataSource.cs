﻿using ConsoleEShopOnion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion
{
    public class DataSource
    {
        public static List<Product> Products { get; set; } = new List<Product>()
        {
             new Product(){ ProductName= "Iphone 12", Price=1199, Description = "Apple Iphone", Category = "Phones"},
             new Product(){ ProductName= "Samsung Galaxy s41", Price=999, Description = "Samsung corporation", Category = "Phones"},
             new Product(){ ProductName= "Mercedes benz E-200", Price=58999, Description = "The best car ever",  Category = "Cars"},
             new Product(){ ProductName= "Apple airPods", Price=199, Description = "Apple Inc", Category = "HeadPhones"}
        };

        public static List<User> Users { get; set; } = new List<User>()
        {
                new RegisteredUser(){Login= "simpleUSer", Password = "password"},
                new RegisteredUser(){Login = "user1", Password = "user1" },
                new RegisteredUser(){Login="admin", Password="admin", Permission=UserPermission.Admin }
        };

        public static List<Order> Orders { get; set; } = new List<Order>();
    }
}
