﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.Infrastructure;

namespace ConsoleEShopOnion.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {

        public DbContext db;
        public int Count { get => db.Users.Count; }

        public UserRepository(DbContext context) 
        {
            db = context;
        }

        public User Find(Predicate<User> p) 
        {
            return db.Users.Find(p);
        }

        public bool UserExist(string login) 
        {
            return db.Users.Exists(i=> i.Login == login);
        }

        public bool UserExist(string login, string password)
        {
            return db.Users.Exists(i => i.Login == login && i.Password == password);
        }

        public bool Create(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("", new Exception());
            }
            if (UserExist(item.Login, item.Password))
                return false;
            item.Id = IDGenerator.GenerateID(db.Users);
            db.Users.Add(item);
            return true;
        }

        public User Get(int id)
        {
            return db.Users.FirstOrDefault(i => i.Id == id);
        }

        public List<User> Get()
        {
            return db.Users;
        }

        public void Update(User item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            if (!(db.Users.Exists(i => i.Id == id)))
            {
                throw new ArgumentException(nameof(this.Delete), new Exception("User with such ID is not exist"));
            }
            db.Users.Remove(db.Users.Find(i => i.Id == id));
        }
    }
}
