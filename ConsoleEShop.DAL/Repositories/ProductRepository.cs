﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.Infrastructure;

namespace ConsoleEShopOnion.DAL.Repositories
{
    public class ProductRepository :  IRepository<Product>
    {

        public DbContext db;

        public int Count { get => db.Products.Count; }


        public ProductRepository(DbContext context)
        {
            db = context;
        }


        public Product Find(Predicate<Product> p)
        {
            return db.Products.Find(p);
        }

        public bool Create(Product item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(this.Create), new Exception());

            if (db.Products.Contains(item))
                return false;

            item.Id = IDGenerator.GenerateID(db.Products);
            db.Products.Add(item);
            return true;
        }

        public Product Get(int id)
        {
            return db.Products.FirstOrDefault(i=> i.Id == id);
        }

        public List<Product> Get()
        {
            return db.Products;
         
        }

        public void Update(Product item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            if(!(db.Products.Exists(i=> i.Id == id))) 
            {
                throw new ArgumentException(nameof(this.Delete), new Exception("Product with such ID is not exist"));
            }
            db.Products.Remove(db.Products.Find(i=>i.Id==id));
        }
    }
}

