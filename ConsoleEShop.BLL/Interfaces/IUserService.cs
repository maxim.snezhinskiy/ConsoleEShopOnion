﻿using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.BLL.Interfaces
{
    public interface IUserService
    {
        public IUnitOfWork UoW { get; set; }
        public void Add(User user);
        public IEnumerable<User> Get();
        public User Get(int id);
        public User Find(Predicate<User> p);
        public User LogIn(string login, string password);
        public void Register(User user);
    }
}
