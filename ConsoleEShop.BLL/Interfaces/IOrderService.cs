﻿using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ConsoleEShop.BLL.DTOs;

namespace ConsoleEShopOnion.BLL.Interfaces
{
    public interface IOrderService
    {
        public IUnitOfWork UoW { get; set; }
        public void Add(OrderDto order);
        public void AddOrderItem(int orderId, OrderItem item);
        public IEnumerable<Order> Get();
        public OrderDto Find(Predicate<Order> p);
        public IEnumerable<OrderDto> GetUserOrders(int userId);
        
    }
}
