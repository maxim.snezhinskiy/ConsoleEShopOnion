﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopOnion.DAL.UoW;
using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShop.BLL.DTOs;

namespace ConsoleEShopOnion.BLL.Interfaces
{
    public interface IProductService 
    {
        public IUnitOfWork UoW { get; set; }

        public bool Add(Product product);
        public ProductDto Find(Predicate<Product> p);
        public Product Get(int id);
        public List<Product> GetAll();
    }
}
