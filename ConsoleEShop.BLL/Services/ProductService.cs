﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopOnion.BLL.Interfaces;
using ConsoleEShopOnion.DAL.UoW;
using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShop.BLL.DTOs;
using AutoMapper;

namespace ConsoleEShopOnion.BLL.Services
{
    public class ProductService : IProductService

    {
        public IUnitOfWork UoW { get ; set; }
        private IMapper mapper;
        public ProductService(IUnitOfWork uow) 
        {
            if(uow == null)
            {
                throw new ArgumentNullException();
            }
            UoW = uow;
            mapper = GetMapperConfiguration().CreateMapper();
        }

        public MapperConfiguration GetMapperConfiguration()
        {
            return new MapperConfiguration(cfg => {
                cfg.CreateMap<Product, ProductDto>();
                cfg.CreateMap<ProductDto, Product>();
            });
        }

        public bool Add(Product product)
        {
            if(product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            return UoW.Products.Create(product);
        }

        public ProductDto Find(Predicate<Product> p)
        {
            if (p == null)
            {
                throw new ArgumentNullException(nameof(Find));
            }
            var pr = UoW.Products.Find(p);
            return mapper.Map<Product, ProductDto>(pr);
        }

        public Product Get(int id)
        {
            return UoW.Products.Get(id);
        }

        public List<Product> GetAll()
        {
            return UoW.Products.Get();
        }
    }
}
