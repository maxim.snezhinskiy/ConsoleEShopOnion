﻿using ConsoleEShopOnion.BLL.Interfaces;
using ConsoleEShopOnion.DAL.Entities;
using ConsoleEShopOnion.DAL.UoW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.BLL.Services
{
    public class UserService : IUserService
    {
        public IUnitOfWork UoW { get; set; }

        public UserService(IUnitOfWork uow)
        {
            if (uow == null)
            {
                throw new ArgumentNullException();
            }
            UoW = uow;
        }
        public void Add(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(Add));
            }
            UoW.Users.Create(user);
        }

        public User Find(Predicate<User> p)
        {
            if (p == null)
            {
                throw new ArgumentNullException(nameof(Add));
            }
            return UoW.Users.Find(p);
        }

        public IEnumerable<User> Get()
        {
            return UoW.Users.Get();
        }

        public User Get(int id)
        {
            return UoW.Users.Get(id);
        }

        public User LogIn(string login, string password)
        {
            User exist = UoW.Users.Find(i => i.Login == login && i.Password == password);
            if (exist == null)
            {
                throw new Exception("Wrong login or password");
            }
            return exist;
        }

        public void Register(User user)
        {
            User exist = UoW.Users.Find(i => i.Login == user.Login && i.Password == user.Password);
            if (exist != null)
            {
                throw new Exception("User with this login is already exist");
            }
            UoW.Users.Create(user);
        }
    }
}
