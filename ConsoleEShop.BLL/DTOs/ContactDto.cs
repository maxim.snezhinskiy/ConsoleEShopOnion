﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop.BLL.DTOs
{
    public class ContactDto
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return $"Phone number: {PhoneNumber ?? "Unknown"}\n" +
                $"Email: {Email ?? "Unknown"}";
        }
    }
}
