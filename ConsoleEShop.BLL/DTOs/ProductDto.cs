﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop.BLL.DTOs
{
    public class ProductDto
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }

        public override string ToString()
        {
            return $"Product ID: {Id} \nName: {ProductName}, price: {Price}$\nDescription: {Description},\nCategory: {Category ?? "Unknown"} ";
        }
    }
}
