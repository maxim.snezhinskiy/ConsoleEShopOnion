﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop.BLL.DTOs
{
    public class OrderItemDto
    {
        public int Quantity { get; set; }
        public ProductDto Product { get; set; }

        public override string ToString()
        {
            return $"\n-{Product}\nQuantity: {Quantity}";
        }

    }
}
