﻿using ConsoleEShopOnion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop.BLL.DTOs
{
     public class UserDto
    {
        public string Name { get; set; }
        private static int ID = 0;
        public UserPermission Permission { get; set; }

        public ContactDto contact { get; set; } = new ContactDto();
        public string Login { get; set; }
        public string Password { get; set; }

        public int Id { get; private set; }

        public override string ToString()
        {
            return $"User ID: {Id}\n" +
                $"Name: {Name ?? "Unknown"}\n" +
                $"Login: {Login}\n" +
                $"{contact?.ToString()}\n";
        }

    }
}
